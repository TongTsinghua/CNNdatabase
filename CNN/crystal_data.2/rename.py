# encoding: utf-8
import os
import os.path

curDir = os.getcwd()
id = 0
crystal_type = 1 #You may need to change this parameter for different lattice type
if crystal_type == 0:
    for parent, dirnames, filenames in os.walk(curDir+'/hexagonal/resized_data/'):  
        for filename in filenames:
                    id+=1
                    newName = 'hexagonal_'+str(id)+'.jpg'
                    os.rename(os.path.join(parent, filename), os.path.join(parent, newName))
                    print(newName)
else:
    for parent, dirnames, filenames in os.walk(curDir+'/square/resized_data/'):  
        for filename in filenames:
                    id+=1
                    newName = 'square_'+str(id)+'.jpg'
                    os.rename(os.path.join(parent, filename), os.path.join(parent, newName))
                    print(newName)

#os.system("pause")  
