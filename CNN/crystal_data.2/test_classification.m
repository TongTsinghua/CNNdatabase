%This is a test code generating test image for crystal classification

%Shengtong Zhang Aug 17/2017
close all;
clear all;

is_show = 0;
R_low2 = 0; R_high2 = 0;
R_low3 = 0; R_high3 = 0;
spExtention = 0;
fqThre = 0;
extention = 0.7;
rad = 1.5;
threBD = 0.3;
energyThre = 2.0;
t_sc = 0.8; s_sc = 0.7;
red = 8;%20
div = 4;
NB = [30,35];
isLake = 0;
gdSzType = 5;
patchSize = 512;

sigma = 0.2; % parameter for spectral clustering
deformFactor = 0.25;
distType = 4;
numAgl = 9;
clsThre = 0.5;
isScale = 1;

N = patchSize;
x=0:1/N:(N-1)/N;
[xo yo] = ndgrid(x);
amp = 0.0;
xx = xo + amp*sin(2*pi*xo);
yy = yo + amp*sin(2*pi*yo);
bmp = 0.0;
alpha = pi/12 + bmp*sin(2*pi*xo) + bmp*cos(2*pi*yo);
F = 50;
phi = exp(2*pi*i*F*(cos(alpha).*xx+sin(alpha).*yy)) + exp(2*pi*i*F*(cos(alpha+pi/3).*xx+sin(alpha+pi/3).*yy)) + exp(2*pi*i*F*(cos(alpha+2*pi/3).*xx+sin(alpha+2*pi/3).*yy));
phi = real(phi)/3;
pos = find(phi<0);
phi(pos) = 0;


phi2 = circshift(phi,3,1);
phi2 = circshift(phi2,4,2);
phi2 = max(phi2,phi);

alpha = -pi/24  + bmp*cos(2*pi*xo) + bmp*sin(2*pi*yo);
phi3 = exp(2*pi*i*F*(cos(alpha).*xx+sin(alpha).*yy)) + exp(2*pi*i*F*(cos(alpha+pi/2).*xx+sin(alpha+pi/2).*yy));
phi3 = (real(phi3)/2);
pos = find(phi3<0);
phi3(pos) = 0;

phi4 = circshift(phi3,4,1);
phi4 = circshift(phi4,4,2);
phi4 = max(phi4,phi3);

phi(end/2+1:end,1:end/2) = phi2(1:end/2,1:end/2);
phi(1:end/2,end/2+1:end) = phi3(1:end/2,1:end/2);
phi(end/2+1:end,end/2+1:end) = phi4(1:end/2,1:end/2);

phi(1:end/4,1:end/4) = -1.5*phi(1:end/4,1:end/4);
phi(1:end/4,3*end/4+1:end) = phi(1:end/4,3*end/4+1:end)*2;
phi(3*end/4+1:end,1:end/4) = phi(3*end/4+1:end,1:end/4)/2;
phi(3*end/4+1:end,3*end/4+1:end) = -0.5*phi(3*end/4+1:end,3*end/4+1:end);


phi = phi/max(max(abs(phi)));
phi = phi - mean(phi(:));
pic = figure;
imagesc(phi);
axis image;set(gca,'xtick',[]);set(gca,'ytick',[]);colormap (gray);
set(gca, 'FontSize', 16);
b=get(gca);
set(b.XLabel, 'FontSize', 16);set(b.YLabel, 'FontSize', 16);set(b.ZLabel, 'FontSize', 16);set(b.Title, 'FontSize', 16);

image_size = 64;
[m,n]=size(phi);
crystal_type=zeros(m,n);
crystal_type(:,floor(n/2)+1:end)=1;


pre_resize(phi, image_size, crystal_type);