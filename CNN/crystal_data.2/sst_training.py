"""
This code is used to determine the type of crystal lattice.
The dataset is hexagonal and square.

Written by Shengtong Zhang
"""


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
import tensorflow as tf
import sst_input_data
import sst_model
"""
N_CLASSES = 2 #Hexagonal and Square
IMG_W = 128  
IMG_H = 128
BATCH_SIZE = 16
CAPACITY = 2000
MAX_STEP = 10000
learning_rate = 0.0001
SST_RATIO=0.25  #The ratio of the result in fourier domain and the spacial domain

train_dir = './crystal_train/'
test_dir = './crystal_test/'
logs_train_dir = './logs/sst_train/'
SST_IMG_W=12
SST_IMG_H=12

train,sst_train,train_label = sst_input_data.get_files(train_dir)

train_batch,sst_batch,train_label_batch=sst_input_data.get_batch(train,
                                sst_train,
                                train_label,
                                IMG_W,
                                IMG_H,
                                SST_IMG_W,
                                SST_IMG_H,
                                BATCH_SIZE, 
                                CAPACITY)


train_logits = sst_model.inference(train_batch, sst_batch, BATCH_SIZE, N_CLASSES, SST_RATIO)
train_loss = sst_model.losses(train_logits, train_label_batch)
train_op = sst_model.trainning(train_loss, learning_rate)
train__acc = sst_model.evaluation(train_logits, train_label_batch)
summary_op = tf.summary.merge_all()


sess = tf.Session()  
train_writer = tf.summary.FileWriter(logs_train_dir, sess.graph) 
saver = tf.train.Saver()
sess.run(tf.global_variables_initializer()) 


coord = tf.train.Coordinator()
threads = tf.train.start_queue_runners(sess=sess, coord=coord)

try:
    for step in np.arange(MAX_STEP):
        if coord.should_stop():
                break
        _, tra_loss, tra_acc = sess.run([train_op, train_loss, train__acc]) 
        if step % 50 == 0:
            print('Step %d, train loss = %.2f, train accuracy = %.2f%%' %(step,
                                                        tra_loss, tra_acc*100.0))
            summary_str = sess.run(summary_op)
            train_writer.add_summary(summary_str, step)
        if step % 2000 == 0 or (step) == MAX_STEP:
            checkpoint_path = os.path.join(logs_train_dir, 'model.ckpt')
            saver.save(sess, checkpoint_path, global_step=step)

except tf.errors.OutOfRangeError:
    print('Done training -- epoch limit reached')
finally:
    coord.request_stop()
    sess.close()
    
#%% Evaluate one image
# when training, comment the following codes.

"""
from PIL import Image
import matplotlib.pyplot as plt

def get_one_image(test, sst):
    '''Randomly pick one image from training data
    Return: ndarray
    '''
    n = len(test)
    ind = np.random.randint(0, n)
    img_dir = test[ind]
    sst_dir = sst[ind]

    image = Image.open(img_dir)
    sst_image = Image.open(sst_dir)
    plt.imshow(image,cmap='gray')
    image = image.resize([128, 128])
    sst_image = sst_image.resize([12,12])
    image = np.array(image)
    sst_image = np.array(sst_image)
    return image,sst_image

def evaluate_one_image():
    '''Test one image against the saved models and parameters
    '''
    # you need to change the directories to yours.
    test_dir = './crystal_test/'
    test, sst, test_label = sst_input_data.get_files(test_dir)
    image_array, sst_array = get_one_image(test, sst)
    
    with tf.Graph().as_default():
        BATCH_SIZE = 1
        N_CLASSES = 2
        SST_RATIO=0.25
        
        image = tf.cast(image_array, tf.float32)
        image = tf.reshape(image, [128, 128, 1])
        image = tf.image.per_image_standardization(image)
        image = tf.reshape(image, [1, 128, 128, 1])

        sst_image = tf.cast(sst_array, tf.float32)
        sst_image = tf.reshape(sst_image, [12, 12,1])
        sst_image = tf.image.per_image_standardization(sst_image)
        sst_image = tf.reshape(sst_image,[1,12,12,1])
        logit = sst_model.inference(image, sst_image, BATCH_SIZE, N_CLASSES, SST_RATIO)
        
        logit = tf.nn.softmax(logit)
        
        x = tf.placeholder(tf.float32, shape=[128, 128])
        sst_x = tf.placeholder(tf.float32, shape = [12,12])
        # you need to change the directories to yours.
        logs_train_dir = './logs/sst_train/' 
                       
        saver = tf.train.Saver()
        
        with tf.Session() as sess:
            
            print("Reading checkpoints...")
            ckpt = tf.train.get_checkpoint_state(logs_train_dir)
            if ckpt and ckpt.model_checkpoint_path:
                global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                saver.restore(sess, ckpt.model_checkpoint_path)
                print('Loading success, global_step is %s' % global_step)
            else:
                print('No checkpoint file found')
            
            prediction = sess.run(logit, feed_dict={x: image_array, sst_x: sst_array})
            max_index = np.argmax(prediction)
            if max_index==0:
                print('This is a hexagonal lattice with possibility %.6f' %prediction[:, 0])
            else:
                print('This is a square lattice with possibility %.6f' %prediction[:, 1])
#test_code:
evaluate_one_image()
plt.axis('off')
plt.show()


