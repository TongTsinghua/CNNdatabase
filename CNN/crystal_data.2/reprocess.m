%generate resized image data in the directory: resized_data
%
%Written by Shengtong Zhang, Aug/23/2017
clear all;
close all;
for id_type = 2
type =id_type-1;  %0:hexagonal,  1:square
id=1;
if(type==1)
    type_name = '.\square\';
    Name='square_';
    is_random = 0;
elseif(type == 0)
    type_name = '.\hexagonal\';
    Name = 'hexagonal_';
    is_random = 0;
end
file{1}=dir(strcat([type_name,'resized_pic\','*.tif']));
file{2}=dir(strcat([type_name,'resized_pic\','*.png']));
file{3}=dir(strcat([type_name,'resized_pic\','*.jpg']));
file{4}=dir(strcat([type_name,'resized_pic\','*.gif']));

image_size=64;
Max_Image=100;

for index = 1:length(file)
    image_num=length(file{index});
    i = 1;
    while(image_num > 0 && i < image_num)
        cnt=0;
        image_name = file{index}(i).name;
        A=double(imread(strcat(type_name,'resized_pic\',image_name)));
        if(length(size(A)) == 3)
            disp('This is a RGB image');
            tmp=A(:,:,1);
            A=tmp;
        end
        A=A/max(abs(A(:)));
        A=A - mean(A(:));
        [m, n] = size(A);
        if(min(m,n) >= image_size)
            nx=floor(m/image_size);
            ny=floor(n/image_size);
            for j = 1 : nx
                for k = 1 : ny
                    if(is_random)
                        iteration_num = 0;
                        while(iteration_num < 11 && cnt < Max_Image)
                            if(m-j*image_size==0 && n-k*image_size==0)%pick the patch randomly
                                a = 0;b=0;
                            elseif(n-k*image_size==0)
                                b=0;
                            elseif(m-j*image_size==0)
                                a=0;
                            else
                                a = randi(min(image_size, m-j*image_size));
                                b = randi(min(image_size, n-k*image_size));
                            end
                            B=A((j-1)*image_size+a+1:a+j*image_size,(k-1)*image_size+b+1:b+k*image_size);
                            %B2=imrotate(B,90);
                            %B3=imrotate(B,180);
                            %B4=imrotate(B,270);
                            %B5=flip(B,1);
                            %B6=flip(B,2);
                            if(max(abs(B(:)))>0)
                                imwrite(B,[[type_name,'resized_data\'], Name,num2str(id), '.jpg']);
%                                 imwrite(B2,[[type_name,'resized_data\'], Name, num2str(id+1), '.jpg']);
%                                 imwrite(B3,[[type_name,'resized_data\'], Name, num2str(id+2), '.jpg']);
%                                 imwrite(B4,[[type_name,'resized_data\'], Name, num2str(id+3), '.jpg']);
                                %imwrite(B5,[[type_name,'resized_data/'], Name, num2str(id+4), '.jpg']);
                                %imwrite(B6,[[type_name,'resized_data/'], Name, num2str(id+5), '.jpg']);
                            else
                                image_name;
                            end
                            id=id+1;
                            cnt = cnt+1;
                            iteration_num = iteration_num+1;
                        end
                    else
                        while(cnt<Max_Image-1)
                            B=A((j-1)*image_size+1:j*image_size,(k-1)*image_size+1:k *image_size);
%                             B2=imrotate(B,90);
%                             B3=imrotate(B,180);
%                             B4=imrotate(B,270);
%                             B5=flip(B,1);
%                             B6=flip(B,2);
                            if(max(abs(B(:)))>0)
                                imwrite(B,[[type_name,'resized_data\'],  Name,num2str(3000+id), '.jpg']);
%                                 imwrite(B2,[[type_name,'resized_data\'], Name,num2str(3000+id+1), '.jpg']);
%                                 imwrite(B3,[[type_name,'resized_data\'], Name, num2str(3000+id+2), '.jpg']);
%                                 imwrite(B4,[[type_name,'resized_data\'], Name, num2str(3000+id+3), '.jpg']);
%                                 imwrite(B5,[[type_name,'resized_data\'], Name, num2str(3000+id+4), '.jpg']);
%                                 imwrite(B6,[[type_name,'resized_data\'], Name, num2str(3000+id+5), '.jpg']);
                            else
                            end
                            id=id+1;
                            cnt=cnt+1;
                        end
                    end
                end
            end
        end
    i=i+1;
    image_name
    end
    %clear A,B;
end
end
        




% div = 2;
% Y = fft2(X);
% for i = 1:floor(N/div)
%     for j = 1:floor(N/div)
%         tmp(i,j)=Y(2*i, 2*j);
%     end
% end
% Y2=tmp;
% X2 =ifft2(Y2);
% imshow(X,[]); colormap(gray)
% figure;imshow(X2,[]); colormap(gray)
