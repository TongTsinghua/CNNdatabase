from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import os

#file_dir = './hexagonal/resized_data/'
#train_dir = './crystal_train/'
#test_dir = './crystal_test/'
def get_files(file_dir):
    '''
    Args:
        file_dir: file directory
    Returns:
        list of images and labels
    '''
    hexagonal = []
    label_hexagonal = []
    square = []
    label_square = []
    for file in os.listdir(file_dir):
        name = file.split('_')
        if name[0]=='hexagonal':
            hexagonal.append(file_dir + file)
            label_hexagonal.append(0)
        else:
            square.append(file_dir + file)
            label_square.append(1)
    print('There are %d hexagonal\nThere are %d square' %(len(hexagonal), len(square)))

    image_list = np.hstack((hexagonal, square))
    label_list = np.hstack((label_hexagonal, label_square))

    temp = np.array([image_list, label_list])
    temp = temp.transpose()
    np.random.shuffle(temp)

    image_list = list(temp[:, 0])
    label_list = list(temp[:, 1])
    label_list = [int(float(i)) for i in label_list]
    return image_list, label_list

def get_batch(image, label, image_W, image_H, batch_size, capacity):
    image = tf.cast(image, tf.string)
    label = tf.cast(label, tf.int32)

    input_queue = tf.train.slice_input_producer([image,label])

    label = input_queue[1]
    image_contents = tf.read_file(input_queue[0])

    image=tf.image.decode_jpeg(image_contents, channels = 1)
    image=tf.image.resize_image_with_crop_or_pad(image, image_W, image_H)
    image=tf.image.per_image_standardization(image)

    image = tf.cast(image, tf.float32)
    image_batch, label_batch = tf.train.batch([image,label],
                                              batch_size = batch_size,
                                              num_threads=64,
                                              capacity=capacity)
    #label_batch = tf.reshape(label_batch, [batch_size])
    return image_batch, label_batch

'''
#test code for input_data.py
BATCH_SIZE = 2
capacity = 256
IMG_W=64
IMG_H=64
train_dir = './crystal_train/'

image_list, label_list = get_files(train_dir)
image_batch,label_batch=get_batch(image_list, label_list, IMG_W, IMG_H, BATCH_SIZE, capacity)
print(image_batch)

with tf.Session() as sess:
    i = 0
    coord = tf.train.Coordinator()
    threads = tf.train.start_queue_runners(coord=coord)
    try:
        while not coord.should_stop() and i<2:
            img, label = sess.run([image_batch, label_batch])
            # just test one batch
            for j in np.arange(BATCH_SIZE):
                print('label: %d' %label[j])
                plt.imshow(img[j,:,:,:])
                plt.show()
            i+=1

    except tf.errors.OutOfRangeError:
        print('done!')
    finally:
        coord.request_stop()
    coord.join(threads)
'''
