clear all;
close all;

type=1;  %0:hexagonal,  1:square
if(type==1)
    type_name = '.\square\resized_data\';
    is_random = 1;
elseif(type == 0)
    type_name = '.\hexagonal\resized_data\';
    is_random = 0;
end
file=dir(strcat([type_name,'*.jpg']));

for i = 1 : length(file)
    image_name = file(i).name;
    A=double(imread(strcat(type_name,image_name)));
    if(length(size(A))==3)
        A=A(:,:,1);
    end
    A = A-min(A(:));
    A=A/max(A(:));
    %pos = find(abs(A(:)) > 0.6);
    pos2 = find(abs(A(:)) < 0.2);
    %A(pos) = 0;
    A(pos2)=0;
    A = smoothImage(A,3,1);
    [BW, threshOut] = edge(A,'sobel', 0.1); 
    if type==0
        imwrite(BW,[type_name, 'hexagonal_' num2str(3000+i), '.jpg']);
    elseif type == 1
        imwrite(BW,[type_name, 'square_' num2str(3000+i), '.jpg']);
    end
    %     threshOut
    %figure; imagesc(A);
    %figure; imagesc(BW);
end