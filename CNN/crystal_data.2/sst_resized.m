%clear all;
%close all;
type = 0;  %0:hexagonal,  1:square
id=1;
if(type==1)
    type_name = './fourier_train_square/';
    type_name_new = './fourier_train_square_new/';
elseif(type == 0)
    type_name = './fourier_train_hexagonal/';
    type_name_new = './fourier_train_hexagonal_new/';
end
file=dir(strcat([type_name,'*.jpg']));

for i = 1:length(file)
    image_name = file(i).name;
    A=double(imread(strcat(type_name,image_name)));
    [m,n]=size(A);
    if(m<n)
        B=[A;zeros(n-m,n)];
    else
        B = A(1:n,:);
    end
    imwrite(B,[type_name_new, image_name]);
    size(B)
end