is_show = 0;
R_low2 = 0; R_high2 = 0;
R_low3 = 0; R_high3 = 0;
spExtention = 0;
fqThre = 0;
extention = 0.7;
rad = 1.5;
threBD = 0.3;
energyThre = 2.0;
t_sc = 0.8; s_sc = 0.7;
red = 8;%20
div = 4;
NB = [30,35];
isLake = 0;
gdSzType = 5;
patchSize = 128;

ex=0;
sigma = 0.2; % parameter for spectral clustering
deformFactor = 0.5;
distType = 4;
numAgl = 12;
clsThre = 0.5;
isScale = 1;
numAtom = 25;
fudgeFactor = .5;

para = struct('sigma',sigma,'gdSzType',gdSzType,'div',div,'NB',NB,'N',patchSize,'red',red,'epsl',1e-4,'is_real',...
    0,'t_sc',t_sc,'s_sc',s_sc,'num_direction',1,'deformFactor',deformFactor,'numAgl',numAgl,'distType',distType,'clsThre',clsThre,...
    'R_low2',R_low2,'R_low3',R_low3,'R_high2',R_high2,'R_high3',R_high3,'SPg',[],'spExtention',spExtention,'extention',extention,...
    'rad',rad,'threBD',threBD,'energyThre',energyThre,'isLake',isLake,'isScale',isScale,'typePick',2,'ex',ex);
A=phiNew;
  A=A-mean(A(:));
            A=A/max(abs(A(:)));
            phi=A;
            [~,info,para] = crystalInfo(phi,para,is_show,numAtom,extention,fudgeFactor,fqThre*2);

            cntm=1; cntn=1;
            phiTemp = phi((1:para.N)+(cntm-1)*para.N/2,(1:para.N)+(cntn-1)*para.N/2);
            % Zero padding for non-periodic image
            [m n] = size(phiTemp);
            dm = 0;
            dn = 0;
            phiTemp = [zeros(dm,dn*2+n);zeros(m,dn) phiTemp zeros(m,dn);zeros(dm,dn*2+n)];
            phiTemp = -phiTemp;

            % Clean peaks in the frequency domain
            [mphi nphi] = size(phiTemp);
            [xg yg] = ndgrid(-mphi/2:mphi/2-1,-nphi/2:nphi/2-1);
            R = sqrt(xg.^2+yg.^2);
            Y = fftshift(fft2(phiTemp));
            pos = find(R>para.R_high3);
            Y(pos) = 0;
            pos = find(R<para.R_low3);
            Y(pos) = 0;
            phiTemp = real(ifft2(ifftshift(Y)));

            % Search for peaks
            R_low = min(para.R_low2,para.R_low3)*(1-para.spExtention); R_high = 2*max(para.R_high2,para.R_high3)*(1+para.spExtention);
            skeletonWhole  = skeletonPolar(para.num_direction,phiTemp,para.SPg,para.NB,para.rad,para.is_real,...
                R_low,R_high,para.epsl,para.red,para.t_sc,para.s_sc,para.deformFactor,para.numAgl,para.isScale);
            szSSTWhole = size(skeletonWhole);
            pos1 = gdSzType:gdSzType:szSSTWhole(3)-gdSzType; pos2 = gdSzType:gdSzType:szSSTWhole(4)-gdSzType;
            skeleton = skeletonWhole(:,:,pos1,pos2);
            szSST = size(skeleton);
            fff_skeleton = skeleton(:,:,floor(szSST(3)/2), floor(szSST(4)/2));