clear all;
close all;
type = 0;  %0:hexagonal,  1:square
id=1;
if(type==1)
    type_name = '.\square\';
    isCheckType = 2;
elseif(type == 0)
    type_name = '.\hexagonal\';
    isCheckType = 3;
end
file{1}=dir(strcat([type_name,'*.tif']));
file{2}=dir(strcat([type_name,'*.png']));
file{3}=dir(strcat([type_name,'*.jpg']));
file{4}=dir(strcat([type_name,'*.gif']));

ImageSize=128;

numFile = 20;
sigma = 30;
numAtomOutlier = 0;
R_low2 = 0; R_high2 = 0;
R_low3 = 0; R_high3 = 0;
spExtention = 0.2;
fqThre = 0;
extention = 0.2;
rad = 1;
AtomPatch = 10;


threBD = 0.0;
energyThre = 2.0;
patchSize = 512;
t_sc = 1; s_sc = 0.8;
isCompDeform = 1; red = [10,4];
fudgeFactor = 0.5; div = 2;
typeBD = 2; NB = 15;
isRectangle = 0;  isLake = 1;
numAtom = 25;

paraSST = struct('div',div,'NB',[1 NB],'N',patchSize,'red',red,'epsl',...
    1e-4,'is_real',0,'t_sc',t_sc,'s_sc',s_sc,'num_direction',1,...
    'R_low2',R_low2,'R_low3',R_low3,'R_high2',R_high2,'R_high3',R_high3,...
    'SPg',[],'spExtention',spExtention,'extention',extention,...
    'rad',rad,'threBD',threBD,'energyThre',energyThre,'isLake',isLake,...
    'isCheckType',isCheckType,'isRectangle',isRectangle,...
    'isCompDeform',isCompDeform','typeBD',typeBD,...
    'numAtomOutlier',numAtomOutlier);

for index = 3 % 1:length(file)
    image_num=length(file{index});
    for i = 1 % 1:image_num
        image_name = file{index}(i).name
        phiwhole = double(imread(strcat(type_name,image_name)));
        if(length(size(phiwhole))==3)
            phiwhole = phiwhole(:,:,1);
        end
        figure;imagesc(phiwhole);
        phiwhole = phiwhole/max(max(abs(phiwhole)));
        phiwhole = phiwhole - mean(phiwhole(:));
        [orgm,orgn] = size(phiwhole);
        m = orgm; n = orgn;           
        [~,info,paraSST] = crystalInfo(phiwhole,paraSST,1,...
        numAtom,extention,fudgeFactor,fqThre*2);
            % info.numPix is the area of numAtom atoms
            %close all;
        rate = floor(AtomPatch*(max(orgm,orgn)/info.peak/div) / ImageSize)
        phiNew=downsample(phiwhole, rate,1);
        figure; imagesc(phiNew(1:ImageSize, 1:ImageSize)), title('patch');
    end
end

