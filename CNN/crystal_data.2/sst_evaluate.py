from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
import math
import tensorflow as tf
import sst_input_data
import sst_model
#import training

from PIL import Image
import matplotlib as mpl
#mpl.use('Agg')

import matplotlib.pyplot as plt
#from scipy.misc import imsave

def get_one_image(img_dir):
    '''Randomly pick one image from training data
    Return: ndarray
    '''
    #n = len(train)
    #ind = np.random.randint(0, n)
    #img_dir = train[0]
    filename = os.path.basename(img_dir)
    name = filename.split('_')
    if name[0] == 'hexagonal':
        sst_dir = './fourier_test_file/'
        sst_name = 'hexagonalFourier_'+name[1]
        sst_dir = sst_dir+sst_name
    else:
        sst_dir = './fourier_test_file/'
        sst_name = 'squareFourier_'+name[1]
        sst_dir = sst_dir+sst_name
    image = Image.open(img_dir)
    sst_image = Image.open(sst_dir)
    #plt.imshow(image)
    image = image.resize([128, 128])
    sst_image = sst_image.resize([12,12])
    image = np.array(image)
    sst_image = np.array(sst_image)
    return image,sst_image

def evaluate_one_image(test_file):
    '''Test one image against the saved models and parameters
    '''
    # you need to change the directories to yours.
    #test_dir = '.\\test_file\\hexagonal_32.32.jpg'
    #test_dir2 = '.\\test_file\\hexagonal_64.32.jpg'
    #test, test_label = input_data.get_files(test_dir)
    image_array, sst_array = get_one_image(test_file)
    
    with tf.Graph().as_default	():
        BATCH_SIZE = 1
        N_CLASSES = 2
        SST_RATIO=0.25
        
        image = tf.cast(image_array, tf.float32)
        image = tf.reshape(image, [128, 128, 1])
        image = tf.image.per_image_standardization(image)
        image = tf.reshape(image, [1, 128, 128, 1])

        sst_image = tf.cast(sst_array, tf.float32)
        sst_image = tf.reshape(sst_image, [12, 12,1])
        sst_image = tf.image.per_image_standardization(sst_image)
        sst_image = tf.reshape(sst_image,[1,12,12,1])
        logit = sst_model.inference(image, sst_image, BATCH_SIZE, N_CLASSES, SST_RATIO)
        
        logit = tf.nn.softmax(logit)
        
        x = tf.placeholder(tf.float32, shape=[128, 128])
        sst_x = tf.placeholder(tf.float32, shape = [12,12])
        # you need to change the directories to yours.
        logs_train_dir = './logs/sst_train/' 
                       
        
        saver = tf.train.Saver()
        
        with tf.Session() as sess:
            
            #print("Reading checkpoints...")
            ckpt = tf.train.get_checkpoint_state(logs_train_dir)
            if ckpt and ckpt.model_checkpoint_path:
                global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                saver.restore(sess, ckpt.model_checkpoint_path)
                #print('Loading success, global_step is %s' % global_step)
            else:
                print('No checkpoint file found')
            
            prediction = sess.run(logit, feed_dict={x: image_array, sst_x: sst_array})
            print(prediction)
            max_index = np.argmax(prediction)
            if max_index==0:
                #print('This is a hexagonal lattice with possibility %.6f' %prediction[:, 0])
                return 0
            else:
                #print('This is a square lattice with possibility %.6f' %prediction[:, 1])
                return 1
                

file_dir='./test_file/'
NUM_CLASS=2
crystal_class=[]
for file in os.listdir(file_dir):
    tmp=evaluate_one_image(file_dir+file)
    crystal_class+=[tmp]
length=len(crystal_class)
size=int(math.sqrt(length))
print(size)
crystal_class=crystal_class[:size**2]
crystal_class=np.reshape(crystal_class, [size,size])
print(crystal_class)
cmap=plt.cm.gray
#norm=mpl.colors.Normalize(vmin=0,vmax=NUM_CLASS-1)
ax = plt.imshow(crystal_class, cmap=cmap)
plt.colorbar(ax,cmap=cmap,ticks=list(range(NUM_CLASS)))
plt.axis('off')
plt.title('crystal classification')

plt.savefig('crystal_class.jpg')
plt.show()
'''   
#test_code:
file = './test_file/hexagonal_64.64.jpg'
print(file)
CC =  evaluate_one_image(file)
print(CC)
#plt.show()
'''
