N=2048;
x=0:1/N:(N-1)/N;
[xo yo] = ndgrid(x);
amp = 0.01;
xx = xo + amp*sin(2*pi*xo);
yy = yo + amp*sin(2*pi*yo);
bmp=0.001*randn(1);
alpha = -pi/11  + bmp*cos(2*pi*xo) + bmp*sin(2*pi*yo);
F = 200;
phi2 = exp(2*pi*i*F/2*(cos(alpha).*xx+sin(alpha).*yy)) + exp(2*pi*i*F/2*(cos(alpha+pi/2).*xx+sin(alpha+pi/2).*yy));
phi2 = (real(phi2)/2);
figure;imagesc(phi2);
%*********************************************************
% dig a hole for a ring
maxVal = max(phi2(:)); meanVal = mean(phi2(:)); threVal = 0.75*maxVal+0.25*meanVal;
pos = find(phi2>=threVal);
phi2(pos) = threVal - 2*(phi2(pos)-threVal);
phi2 = phi2-min(phi2(:));
phi2=phi2/max(phi2(:));
figure;imagesc(phi2(1:128,1:128));
imwrite(phi2,'.\square\ring11.jpg');