from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
import math
import tensorflow as tf
import input_data
import model
#import training

from PIL import Image
import matplotlib as mpl
#mpl.use('Agg')

import matplotlib.pyplot as plt
#from scipy.misc import imsave

def get_one_image(img_dir):
    '''Randomly pick one image from training data
    Return: ndarray
    '''
    #n = len(train)
    #ind = np.random.randint(0, n)
    #img_dir = train[0]

    image = Image.open(img_dir)
    plt.imshow(image)
    image = image.resize([64, 64])
    image = np.array(image)
    return image

def evaluate_one_image(test_file):
    '''Test one image against the saved models and parameters
    '''
    # you need to change the directories to yours.
    #test_dir = '.\\test_file\\hexagonal_32.32.jpg'
    #test_dir2 = '.\\test_file\\hexagonal_64.32.jpg'
    #test, test_label = input_data.get_files(test_dir)
    image_array = get_one_image(test_file)
    
    with tf.Graph().as_default	():
        BATCH_SIZE = 1
        N_CLASSES = 2
        image = tf.reshape(image_array, [1,64, 64])
        image = tf.image.per_image_standardization(image)
        image = tf.reshape(image, [1, 64, 64, 1])
        logit = model.inference(image, BATCH_SIZE, N_CLASSES)
        
        logit = tf.nn.softmax(logit)
        
        x = tf.placeholder(tf.float32, shape=[64, 64])
        
        # you need to change the directories to yours.
        logs_train_dir = './logs/train/' 
                       
        saver = tf.train.Saver()
        
        with tf.Session() as sess:
            
            #print("Reading checkpoints...")
            ckpt = tf.train.get_checkpoint_state(logs_train_dir)
            if ckpt and ckpt.model_checkpoint_path:
                global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                saver.restore(sess, ckpt.model_checkpoint_path)
                #print('Loading success, global_step is %s' % global_step)
            else:
                print('No checkpoint file found')
            
            prediction = sess.run(logit, feed_dict={x: image_array})
            print(prediction)
            max_index = np.argmax(prediction)
            if max_index==0:
                #print('This is a hexagonal lattice with possibility %.6f' %prediction[:, 0])
                return 0
            else:
                #print('This is a square lattice with possibility %.6f' %prediction[:, 1])
                return 1
"""
evaluate_one_image('./hexagonal/t.jpg')
plt.show()
"""
file_dir='F:\Duke\project\crystal_data.2\\test_file\\'
NUM_CLASS=2
crystal_class=np.mat(np.zeros([16,16]))
for file in os.listdir(file_dir):
    name = file.split('.')
    num=name[0].split('_')[1]
    num_x = int(num.split('~')[0])
    num_y = int(num.split('~')[1])
    print(num_x, num_y)
    tmp=evaluate_one_image(file_dir+file)
    crystal_class[num_x-1, num_y-1]=tmp
print(np.shape(crystal_class))

print(crystal_class)
cmap=plt.cm.gray
norm=mpl.colors.Normalize(vmin=0,vmax=NUM_CLASS-1)
ax = plt.imshow(crystal_class, cmap=cmap)
plt.colorbar(ax,cmap=cmap,ticks=list(range(NUM_CLASS)))
#plt.axis('off')
plt.title('crystal classification')

#plt.savefig('crystal_class.jpg')
plt.show()

