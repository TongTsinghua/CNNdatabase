function [phiNew] = downsample(phi, rate, isShow)
if (length(size(phi)) == 3)
    phi = phi(:,:,1)
end
if rate < 1
    disp('The downsampling rate is too small and take it to be 1 automatically.');
    rate = 1;
end
Y=fftshift(fft2(phi));
[m,n] = size(phi);
rate = floor(rate);
phiNew = real(ifft2(ifftshift(Y(floor(m/2)-floor(m/(2*rate))+1:floor(m/2)+floor(m/(2*rate)),...
                                floor(n/2)-floor(n/(2*rate))+1:floor(n/2)+floor(n/(2*rate))))));
if(isShow)
    figure;imagesc(phiNew); title('Downsampling Image');
end