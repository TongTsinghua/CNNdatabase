"""
This code is used to determine the type of crystal lattice.
The dataset is hexagonal and square.

Written by Shengtong Zhang
"""


from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import numpy as np
import tensorflow as tf
import input_data
import model

N_CLASSES = 2 #Hexagonal and Square
IMG_W = 64
IMG_H = 64
BATCH_SIZE = 16
CAPACITY = 2000
MAX_STEP = 4000
learning_rate = 0.0001 

train_dir = './crystal_train/'
test_dir = './crystal_test/'
logs_train_dir = './logs/train/'

train, train_label = input_data.get_files(train_dir)
train_batch,train_label_batch=input_data.get_batch(train,
                                train_label,
                                IMG_W,
                                IMG_H,
                                BATCH_SIZE, 
                                CAPACITY)

train_logits = model.inference(train_batch, BATCH_SIZE, N_CLASSES)
train_loss = model.losses(train_logits, train_label_batch)        
train_op = model.trainning(train_loss, learning_rate)
train__acc = model.evaluation(train_logits, train_label_batch)

summary_op = tf.summary.merge_all()

sess = tf.Session()  
train_writer = tf.summary.FileWriter(logs_train_dir, sess.graph) 
saver = tf.train.Saver()
sess.run(tf.global_variables_initializer()) 


coord = tf.train.Coordinator()
threads = tf.train.start_queue_runners(sess=sess, coord=coord)

try:
    for step in np.arange(MAX_STEP):
        if coord.should_stop():
                break
        _, tra_loss, tra_acc, = sess.run([train_op, train_loss, train__acc]) 
        if step % 50 == 0:
            print('Step %d, train loss = %.2f, train accuracy = %.2f%%' %(step,
                                                        tra_loss, tra_acc*100.0))
            summary_str = sess.run(summary_op)
            train_writer.add_summary(summary_str, step)
        if step % 2000 == 0 or (step) == MAX_STEP:
            checkpoint_path = os.path.join(logs_train_dir, 'model.ckpt')
            saver.save(sess, checkpoint_path, global_step=step)

except tf.errors.OutOfRangeError:
    print('Done training -- epoch limit reached')
finally:
    coord.request_stop()
    sess.close()
    
#%% Evaluate one image
# when training, comment the following codes.

"""
from PIL import Image
import matplotlib.pyplot as plt

def get_one_image(train):
    '''Randomly pick one image from training data
    Return: ndarray
    '''
    n = len(train)
    ind = np.random.randint(0, n)
    img_dir = train[0]

    image = Image.open(img_dir)
    plt.imshow(image,cmap='gray')
    image = image.resize([64, 64])
    image = np.array(image)
    return image

def evaluate_one_image():
    '''Test one image against the saved models and parameters
    '''
    # you need to change the directories to yours.
    test_dir = './crystal_test/'
    test, test_label = input_data.get_files(test_dir)
    image_array = get_one_image(test)
    
    with tf.Graph().as_default():
        BATCH_SIZE = 1
        N_CLASSES = 2
        
        image = tf.cast(image_array, tf.float32)
        image = tf.reshape(image, [64, 64, 1])
        image = tf.image.per_image_standardization(image)
        image = tf.reshape(image, [1, 64, 64, 1])
        logit = model.inference(image, BATCH_SIZE, N_CLASSES)
        
        logit = tf.nn.softmax(logit)
        
        x = tf.placeholder(tf.float32, shape=[64, 64])
        
        # you need to change the directories to yours.
        logs_train_dir = './logs/train/' 
                       
        saver = tf.train.Saver()
        
        with tf.Session() as sess:
            
            print("Reading checkpoints...")
            ckpt = tf.train.get_checkpoint_state(logs_train_dir)
            if ckpt and ckpt.model_checkpoint_path:
                global_step = ckpt.model_checkpoint_path.split('/')[-1].split('-')[-1]
                saver.restore(sess, ckpt.model_checkpoint_path)
                print('Loading success, global_step is %s' % global_step)
            else:
                print('No checkpoint file found')
            
            prediction = sess.run(logit, feed_dict={x: image_array})
            max_index = np.argmax(prediction)
            if max_index==0:
                print('This is a hexagonal lattice with possibility %.6f' %prediction[:, 0])
            else:
                print('This is a square lattice with possibility %.6f' %prediction[:, 1])
#test_code:
evaluate_one_image()
plt.axis('off')
plt.show()

"""
