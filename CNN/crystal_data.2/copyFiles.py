import tensorflow as tf
import numpy as np
import os
import shutil
crystal_type = 1 #0:hexagonal, 1:square
if crystal_type == 0:
    file_dir = './hexagonal/resized_data/'
else:
    file_dir = './square/resized_data/'
train_dir = './crystal_train/'
test_dir = './crystal_test/'
if not os.path.exists(train_dir):
    os.makedirs(train_dir)
if not os.path.exists(test_dir):
    os.makedirs(test_dir)
for file in os.listdir(file_dir):
    sourceFile = os.path.join(file_dir, file)
    name = file.split('.')
    num = int(name[0].split('_')[1])
    if num<=3e4:
        if num%5==0:
            targetFile = os.path.join(test_dir, file)
            if os.path.isfile(sourceFile):
                shutil.copy(sourceFile, test_dir)
        else:
            targetFile = os.path.join(train_dir, file)
            if os.path.isfile(sourceFile):
                shutil.copy(sourceFile, train_dir)
print('moving files done')
