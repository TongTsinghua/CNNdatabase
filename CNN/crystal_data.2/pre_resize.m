function []=pre_resize(image, image_size, crystal_type)
%This function gets the patch on each pixel with image size: image_size

%Shengtong Zhang Aug 17/2017
if(length(size(image))>2) %transform into gray image
    image=image(:,:,1); 
end
[m,n]=size(image);
if(min(m,n)<image_size)
    error('the picture size is too small, it should be no less than %d', image_size);
end
boundary_length = floor(image_size/2);
A=exist('test_file', 'dir');
if(~A)
    disp('making a new directory named test_file')
    mkdir test_file
end

NumSample=32;
for j = 1:m
    for k = 1:n
        if(crystal_type(j,k) == 0)
            type_name='.\test_file\hexagonal_';
        elseif(crystal_type(j,k) == 1)
            type_name='.\test_file\square_';
        end
        if(mod(j,NumSample) == 0 && mod(k,NumSample) == 0)
            j_range=mod([j+1-boundary_length:j+boundary_length], m);
            index_j = find(j_range==0);
            j_range(index_j)=m;
            k_range=mod([k+1-boundary_length:k+boundary_length], n);
            index_k = find(k_range==0);
            k_range(index_k)=n;
            B=image(j_range,k_range);
            B=B-min(B(:));
            B=B/max(B(:));
            imwrite(B,[type_name, num2str(j/NumSample),'~', num2str(k/NumSample) '.jpg']);
            disp([type_name,num2str(j/NumSample),'~', num2str(k/NumSample) '.jpg']);
        end
    end
end