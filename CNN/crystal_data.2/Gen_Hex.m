clear all;
close all;
sigma = 0.2; % parameter for spectral clustering
deformFactor = 0.25;
distType = 4;
numAgl = 9;
clsThre = 0.5;
isScale = 1;

AglNum=30;
cnt=0;
for k = 1:6
    for j = 1 : 40
        for id = 1:AglNum
            theta = id*pi/AglNum;
            N = 512;
            x=0:1/N:(N-1)/N;
            [xo yo] = ndgrid(x);
            amp = 0.01*rand(1);
            xx = xo + amp*sin(2*pi*xo);
            yy = yo + amp*sin(2*pi*yo);
            bmp = 0.01*rand(1);
            alpha = theta + bmp*sin(2*pi*xo) + bmp*cos(2*pi*yo);
            F = (30+j);
            phi = exp(2*pi*i*F*(cos(alpha).*xx+sin(alpha).*yy)) + exp(2*pi*i*F*(cos(alpha+pi/3).*xx+sin(alpha+pi/3).*yy)) + exp(2*pi*i*F*(cos(alpha+2*pi/3).*xx+sin(alpha+2*pi/3).*yy));
            phi = real(phi)/3;
            phi = phi-min(phi(:));
            phi = phi/max(phi(:));
            %imagesc(phi(1:64,1:64));
            phi= smoothImage(phi,3,1);
            pos = find(phi < 0.1*(k-1));
            phi(pos) = 0;
            imwrite(phi,['.\hexagonal\resized_pic\square_', num2str(cnt), '.jpg']);
            phi=phi*(-1);
            phi = phi-min(phi(:));
            phi = phi/max(phi(:));
            imwrite(phi,['.\hexagonal\resized_pic\square_', num2str(cnt+1), '.jpg']);
            cnt = cnt + 2
        end
    end
end

